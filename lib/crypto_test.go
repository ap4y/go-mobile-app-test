package lib

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGenerateKeypair(t *testing.T) {
	f, err := ioutil.TempFile("", "rsa_json")
	require.Nil(t, err)
	defer os.Remove(f.Name())

	err = GenerateKeypair("12345678", f.Name())
	require.Nil(t, err)

	out := map[string]string{}
	err = json.NewDecoder(f).Decode(&out)
	require.Nil(t, err)

	assert.NotNil(t, out["public"])
	assert.NotNil(t, out["private"])
}

func TestGenerateKeypairError(t *testing.T) {
	err := GenerateKeypair("1234", "")
	require.NotNil(t, err)
	assert.Equal(t, "outPath can't be empty", err.Error())

	err = GenerateKeypair("1234", "foo")
	require.NotNil(t, err)
	assert.Equal(t, "invalid pin code length. expected 8. got 4", err.Error())
}
