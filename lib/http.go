package lib

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type Client struct {
	gpsEndpointURL string
}

func NewClient(gpsEndpoingURL string) *Client {
	return &Client{gpsEndpoingURL}
}

func (c *Client) PostLocation(lat, lng string) error {
	body, err := json.Marshal(map[string]string{"latitude": lat, "longitude": lng})
	if err != nil {
		return fmt.Errorf("failed to marshal json: %s", err)
	}

	res, err := http.Post(c.gpsEndpointURL, "application/json", bytes.NewReader(body))
	if err != nil || res.StatusCode != http.StatusOK {
		return fmt.Errorf("http POST failed: %s", err)
	}

	return nil
}
