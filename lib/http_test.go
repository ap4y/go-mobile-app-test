package lib

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPostLocation(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body := map[string]string{}
		err := json.NewDecoder(r.Body).Decode(&body)
		r.Body.Close()

		require.Nil(t, err)
		assert.Equal(t, "-10.00001", body["latitude"])
		assert.Equal(t, "100.124578", body["longitude"])
		assert.Equal(t, "application/json", r.Header.Get("Content-Type"))

		fmt.Fprintln(w, "OK")
	}))
	defer ts.Close()

	client := &Client{ts.URL}
	client.PostLocation("-10.00001", "100.124578")
}
